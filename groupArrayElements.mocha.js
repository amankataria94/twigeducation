/* eslint-env node, mocha */
import { expect } from 'chai'
import groupArrayElements from './groupArrayElements'

describe('groupArrayElements', () => {
    it('should return the correct result on valid input', () => {
        let result = groupArrayElements([1, 4, 5, 6, 7, 8, 9], 3)
        const expectedResult = [[1,4,5], [6,7,8], [9]];
        expect(result).to.deep.equal(expectedResult);
    })
});
