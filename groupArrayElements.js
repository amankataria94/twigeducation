module.exports = (elements, stepSize) => {
    let result = [];
    let currentGroup = [];
    let count = 0;
    for(let i = 0; i < elements.length; i++) {
        currentGroup.push(elements[i]);
        count++;
        if(count >= stepSize || i === elements.length - 1) {
            count = 0;
            result.push(currentGroup);
            currentGroup = [];
        }
    }
    return result;
}