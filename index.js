const program = require('commander');
const chalk = require('chalk');
const groupArrayElements = require('./groupArrayElements');

program
  .version('0.1.0')
  .option('-e, --elements <value>', 'comma seperated list of values')
  .option('-s, --stepSize <value>', 'step size to group by')
  .parse(process.argv);

if(!program.elements) {
    console.log(chalk.red('Please supply elements to be grouped'));
    process.exit(1);
}

if(!program.stepSize || !parseInt(program.stepSize)) {
    console.log(chalk.red('Please valid step size'));
    process.exit(1);
}

const elementArray = program.elements.split(',');
const result = groupArrayElements(elementArray, program.stepSize);
console.log(chalk.green('The grouped elements are:'));
console.log(result);